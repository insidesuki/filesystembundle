<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Service;

use Insidesuki\Bundle\FileSystemWrapper\Exception\DirectoryAlreadyExistsException;
use League\Flysystem\FilesystemException;
use League\Flysystem\UnableToCreateDirectory;
use Insidesuki\Bundle\FileSystemWrapper\Exception\GenericFilesystemException;

class CreateDirectory extends AbstractStorage
{

    /**
     * @throws GenericFilesystemException|FilesystemException
     */
    public function __invoke(string $directory, array $config = [])
    {

        // check if directory already exists
        if($this->storage->directoryExists($directory)){
            throw new DirectoryAlreadyExistsException($directory);
        }

        try{
            $this->storage->createDirectory($directory,$config);
        }
        catch(GenericFilesystemException | UnableToCreateDirectory $ex){
            throw new GenericFilesystemException($ex->getMessage());
        }


    }

}