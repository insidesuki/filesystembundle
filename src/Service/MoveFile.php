<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Service;

use Insidesuki\Bundle\FileSystemWrapper\Exception\FileDoesNotExistsException;
use Insidesuki\Bundle\FileSystemWrapper\Exception\GenericFilesystemException;
use League\Flysystem\FilesystemException;
use League\Flysystem\UnableToMoveFile;


/**
 * Move o rename a file
 */
class MoveFile extends AbstractStorage
{


    private string $source;
    private string $destination;

    /**
     * @throws FilesystemException
     */
    public function __invoke(string $source, string $destination, array $config = [])
    {

        $this->source = $source;
        $this->destination = $destination;
        
        $this->check();

        try{
            $this->storage->move($this->source,$this->destination,$config);
        }
        catch(FilesystemException | UnableToMoveFile $exception){
            throw new GenericFilesystemException($exception->getMessage());
        }

    }

    /**
     * @throws FilesystemException
     */
    private function check()
    {
    // check if source exists
        if(!$this->storage->fileExists($this->source)){
            throw new FileDoesNotExistsException($this->source);
        }

    }

}