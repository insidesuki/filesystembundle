<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Service;

use League\Flysystem\FilesystemException;

class Base64ImgConverter extends AbstractStorage
{


    /**
     * @throws FilesystemException
     */
    public function __invoke(string $base64,
                             string $targetPath,
                             string $fileName): string
    {

        // extract image data from base64 data string
        $pattern = '/data:image\/(.+);base64,(.*)/';
        preg_match($pattern, $base64, $matches);

        // image file extension
        $imageExtension = $matches[1];


        // base64-encoded image data
        $encodedImageData = $matches[2];

        $decodedImageData = base64_decode($encodedImageData);

        $this->createTarget($targetPath);

        $filenameTarget = $targetPath . '/' . $fileName;
        $this->storage->write($filenameTarget, $decodedImageData);


        return $filenameTarget;


    }

    /**
     * @param string $targetPath
     * @return void
     * @throws FilesystemException
     */
    private function createTarget(string $targetPath): void
    {
        // only if directory dont exists
        if (!$this->storage->directoryExists($targetPath)) {
            $createDirectory = new CreateDirectory($this->storage);
            $createDirectory($targetPath);
        }
    }


}