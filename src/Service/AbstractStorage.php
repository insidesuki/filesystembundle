<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Service;

use League\Flysystem\FilesystemOperator;

abstract class AbstractStorage
{

    protected FilesystemOperator $storage;

    public function __construct(FilesystemOperator $storage)
    {

        $this->storage = $storage;

    }

}