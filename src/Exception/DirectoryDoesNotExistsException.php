<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Exception;
use RuntimeException;

class DirectoryDoesNotExistsException extends RuntimeException
{

    public function __construct(string $message)
    {
        parent::__construct(sprintf('The directory "%s" does not exists!!!',$message));
    }

}