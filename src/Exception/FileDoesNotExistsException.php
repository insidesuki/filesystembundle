<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Exception;

class FileDoesNotExistsException extends \RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct(sprintf('The file "%s" does not exists!!!',$message));
    }

}