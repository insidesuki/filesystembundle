<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Exception;

class FileAlreadyExistsException extends \RuntimeException
{
    public function __construct(string $message)
    {
        parent::__construct(sprintf('The file "%s" already exists !!!',$message));
    }

}