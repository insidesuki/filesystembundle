<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Exception;

use RuntimeException;

class GenericFilesystemException extends RuntimeException
{

}