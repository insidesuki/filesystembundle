<?php
declare(strict_types=1);

namespace Insidesuki\Bundle\FileSystemWrapper\Exception;

class DirectoryAlreadyExistsException extends \RuntimeException
{

    public function __construct(string $message)
    {
        parent::__construct(sprintf('The directory "%s" already exists!!!',$message));
    }
}