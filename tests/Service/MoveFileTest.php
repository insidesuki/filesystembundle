<?php

namespace Service;

use Insidesuki\Bundle\FileSystemWrapper\Exception\FileDoesNotExistsException;
use Insidesuki\Bundle\FileSystemWrapper\Service\MoveFile;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use PHPUnit\Framework\TestCase;

class MoveFileTest extends TestCase
{

    private Filesystem $filesystem;

    public function setUp(): void
    {
        $this->filesystem = new Filesystem(
            new InMemoryFilesystemAdapter()
        );

    }

    /**
     * @throws FilesystemException
     */
    public function testIfFailWhenSourceDontExists()
    {

        $this->expectException(FileDoesNotExistsException::class);
        $moveFile = new MoveFile($this->filesystem);
        $moveFile('data/dont', 'data/donte');

    }

    /**
     * @throws FilesystemException
     */
    public function testIfFileWasRenamed()
    {

        // first create a file
        $file = 'data.txt';
        $renamed = 'data_new_name.txt';
        $content = 'some content';
        $this->filesystem->write($file, $content);

        $moveFile = new MoveFile($this->filesystem);
        $moveFile($file, $renamed);

        $this->assertTrue($this->filesystem->fileExists($renamed));

    }

    public function testIfFileWasMovedToAnotherDirectory()
    {

        // first create a file
        $file = 'data.txt';
        $newDirectory = 'data-new';
        $newLocation = $newDirectory.'/'.$file;
        $content = 'some content';
        $this->filesystem->write($file, $content);

        $newdir = $this->filesystem->createDirectory($newDirectory);


        $moveFile = new MoveFile($this->filesystem);
        $moveFile($file, $newLocation);

        $this->assertTrue($this->filesystem->fileExists($newLocation));


    }

}
