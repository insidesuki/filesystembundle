<?php

namespace Service;

use Insidesuki\Bundle\FileSystemWrapper\Exception\DirectoryAlreadyExistsException;
use Insidesuki\Bundle\FileSystemWrapper\Service\CreateDirectory;
use League\Flysystem\Filesystem;
use League\Flysystem\FilesystemException;
use League\Flysystem\InMemory\InMemoryFilesystemAdapter;
use PHPUnit\Framework\TestCase;


class CreateDirectoryTest extends TestCase
{

    private Filesystem $filesystem;


    public function setUp(): void
    {
        $this->filesystem = new Filesystem(
            new InMemoryFilesystemAdapter()
        );

    }

    /**
     * @throws FilesystemException
     */
    public function testIfDirWasCreated()
    {


        $createDirectory = new CreateDirectory($this->filesystem);
        $createDirectory('test');
        $this->assertTrue($this->filesystem->directoryExists('test'));

    }

    public function testIfFailWhenDirectoryExists()
    {
        $createDirectory = new CreateDirectory($this->filesystem);
        $createDirectory('test');


        $this->expectException(DirectoryAlreadyExistsException::class);
        $createDirectory1 = new CreateDirectory($this->filesystem);
        $createDirectory1('test');



    }


}
